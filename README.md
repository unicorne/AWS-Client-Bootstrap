# AWS Client Bootstrap

Automation tools to bootstrap client account and give access to Unicorn Powered team in client environnement.

## How it works

![architecture](images/AWS-Client-Bootstrap.png)

## Client installation documentation

See the client documentation [here](ClientDocumentation.md).
