var request = require('request');
console.log('Loading function');

exports.handler = (event, context, callback) => {
    console.log('event:', event);
    console.log('context:', context);
    console.log('process.env:', process.env);

    const snsMessage = event.Records && event.Records[0].Sns.Message;
    console.log('From SNS:', snsMessage);

    request.post(process.env.EndpointUrl,
        {
            json: {
                AccountId: process.env.AccountId,
                CustomerName: process.env.CustomerName,
                RoleName: process.env.RoleName,
                RoleNameReadOnly: process.env.RoleNameReadOnly,
                SnsMessage: snsMessage,
            }
        },
        // Callback
        (error, response, body) => {
            console.log('error:', error);
            console.log('response:', response);
            console.log('body:', body);
        }
    );
};
