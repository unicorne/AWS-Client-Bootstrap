from chalice import Chalice
import boto3
import botocore
import json

app = Chalice(app_name='aws-unicorn')

cloudformation = boto3.client('cloudformation')  # pylint: disable=C0103
ses = boto3.client('ses', region_name="us-east-1")

boto3.set_stream_logger('boto3', level=boto3.logging.DEBUG)
boto3.set_stream_logger('botocore', level=boto3.logging.DEBUG)
boto3.set_stream_logger('boto3.resources', level=boto3.logging.DEBUG)


template_body = '''
Description:
    This template deploys a UnicornRemoteAccessGroup
    to give unicorns access to customer's environment.

Parameters:

  AccountId:
    Description: Enter the AccountId of the client.
    Type: String

  CustomerName:
    Description: Enter the customer name.
    Type: String

  RoleName:
    Description: Enter the IAM role name for remote access.
    Type: String


Resources:

  UnicornRemoteAccessGroup:
    Type: AWS::IAM::Group
    Properties:
      GroupName: !Sub UnicornRemoteAccessGroup-${CustomerName}
      Policies:
      - PolicyName: !Sub UnicornRemoteAccessPolicy-${CustomerName}
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action: sts:AssumeRole
            Resource: !Sub arn:aws:iam::${AccountId}:role/${RoleName}


Outputs:

  AccountId:
    Description: The customer AccountId
    Value: !Ref AccountId

  CustomerName:
    Description: The customer name
    Value: !Ref CustomerName

  RoleName:
    Description: The role name
    Value: !Ref RoleName
'''

@app.route('/')
def index():
    print(f'version: 1.0')
    return {'version': '1.0'}

# Sample of message
# {
#    'AccountId':'079928462320',
#    'RoleName':'UnicornAccessRole',
#    'SnsMessage':"
#    StackId='arn:aws:cloudformation:ca-central-1:079928462320:stack/wqseqwe/0ee5ab40-761a-11e8-8f05-504dce3aaf36'\n
#    Timestamp='2018-06-22T12:44:52.631Z'\n
#    EventId='0ee64780-761a-11e8-8f05-504dce3aaf36'\n
#    LogicalResourceId='wqseqwe'\n
#    Namespace='079928462320'\n
#    PhysicalResourceId='arn:aws:cloudformation:ca-central-1:079928462320:stack/wqseqwe/0ee5ab40-761a-11e8-8f05-504dce3aaf36'\n
#    PrincipalId='079928462320'\n
#    ResourceProperties='null'\n
#    ResourceStatus='CREATE_IN_PROGRESS'\n
#    ResourceStatusReason='User Initiated'\n
#    ResourceType='AWS::CloudFormation::Stack'\n
#    StackName='wqseqwe'\n
#    ClientRequestToken='Console-CreateStack-0b3d3c51-9d5a-4599-a64f-00ed761214be'\n"
# }

@app.route('/clients', methods=['POST'])
def create_clients():
    # This is the JSON body the user sent in their POST request.
    user_as_json = app.current_request.json_body
    print(user_as_json)


    account_id = user_as_json['AccountId']
    customer_name = _get_valid_customer_name(user_as_json['CustomerName'])
    role_name = user_as_json['RoleName']
    sns = _getMessageDict(user_as_json['SnsMessage'])
    print(f'sns: {sns}')

    status = 'NA'
    print(f'sns ResourceType : "{sns["ResourceType"]}"')
    if sns['ResourceType'] == 'AWS::CloudFormation::Stack':
        print(f'sns ResourceStatus : "{sns["ResourceStatus"]}"')

        if sns['ResourceStatus'] == 'CREATE_COMPLETE':
            _create_or_update_stack(customer_name, account_id, role_name)
            status = 'CREATE_COMPLETE'
        elif sns['ResourceStatus'] == 'UPDATE_COMPLETE':
            _create_or_update_stack(customer_name, account_id, role_name)
            status = 'UPDATE_COMPLETE'
        elif sns['ResourceStatus'] == 'DELETE_COMPLETE':
            status = 'DELETE_COMPLETE'
            _delete_stack_if_exist(customer_name)
        else:
            # status not usefull, ie CREATE_IN_PROGRESS
            status = 'NOTHING_TODO'
    else:
       status = 'NOT_A_STACK_EVENT'

    print(f'status: {status}')
    return {'status': status}


def _getMessageDict(snsMessage):
    snsDict = {}
    snsMessageSplit = snsMessage.split('\n')[:-1]

    for entry in snsMessageSplit:
        [key, value] = entry.split('=')
        snsDict[key] = value.replace("'", "")
    return snsDict


def _create_or_update_stack(customer_name, account_id, role_name):
    print(f'_create_or_update_stack("{customer_name}", "{account_id}", "{role_name}")')
    stack_name = _get_stack_name(customer_name)

    params = {
        'StackName': stack_name,
        'TemplateBody': template_body,
        'Parameters': [
            {
                'ParameterKey': 'AccountId',
                'ParameterValue': account_id,
            },
            {
                'ParameterKey': 'CustomerName',
                'ParameterValue': customer_name,
            },
            {
                'ParameterKey': 'RoleName',
                'ParameterValue': role_name,
            }
        ],
        'Capabilities': ['CAPABILITY_NAMED_IAM'],
        'Tags':[
            {
                'Key': 'BootStrapClient',
                'Value': customer_name
            }
        ]
    }

    try:
        if _stack_exists(stack_name):
            print(f'Updating {stack_name}')
            stack_result = cloudformation.update_stack(**params)
            waiter = cloudformation.get_waiter('stack_update_complete')
            _send_email(
                f"{customer_name} updated shared AccessRole to Unicorn Powered!",
                f"Updated values : \n"
                f"Customer Name : {customer_name}\n"
                f"Account Id : {account_id}\n"
                f"Role Name : {role_name}"
            )
        else:
            print(f'Creating {stack_name}')
            stack_result = cloudformation.create_stack(**params)
            waiter = cloudformation.get_waiter('stack_create_complete')
            _send_email(
                f"{customer_name} shared AccessRole to Unicorn Powered!",
                f"Customer Name : {customer_name}\n"
                f"Account Id : {account_id}\n"
                f"Role Name : {role_name}\n\n"
                f"[profile {customer_name}]\n"
                f"region = ca-central-1\n"
                f"role_arn = arn:aws:iam::{account_id}:role/{role_name}\n"
                f"source_profile = default\n"
            )
        print('...waiting for stack to be ready...')
        waiter.wait(StackName=stack_name)
    except botocore.exceptions.ClientError as ex:
        error_message = ex.response['Error']['Message']
        if error_message == 'No updates are to be performed.':
            print("No changes")
        else:
            raise
    else:
        print(cloudformation.describe_stacks(StackName=stack_result['StackId']))


def _get_valid_customer_name(customer_name):
    return ''.join(ch for ch in customer_name if ch.isalnum())


def _get_stack_name(customer_name):
    return f'BootStrap-{customer_name}'


def _stack_exists(stack_name):
    try:
        stacks = cloudformation.describe_stacks(StackName=stack_name)['Stacks']
        for stack in stacks:
            print(f'Stack status: {stack["StackStatus"]}')
            if stack['StackStatus'] == 'DELETE_COMPLETE':
                continue
            else:
                return True

    except botocore.exceptions.ClientError as e:
        error_message = e.response['Error']['Message']
        if error_message == f'Stack with id {stack_name} does not exist':
            return False
        else:
            raise

    return False


def _delete_stack_if_exist(customer_name):
    stack_name = _get_stack_name(customer_name)

    if _stack_exists(stack_name):
        stack_result = cloudformation.delete_stack(StackName=stack_name)
        waiter = cloudformation.get_waiter('stack_delete_complete')
        _send_email(
            f"Bye Bye {customer_name}",
            f"{customer_name} stopped to share AccessRole to Unicorn Powered."
        )
        print('...waiting for stack to be deleted...')
        waiter.wait(StackName=stack_name)
        print('stack deleted')

def _send_email(subject, body_text):
    SENDER = "bootstrap_client@unicornpowered.com"
    RECIPIENT = "aws@unicornpowered.com"
    CHARSET = "UTF-8"

    try:
        response = ses.send_email(
            Destination={
                'ToAddresses': [
                    RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Text': {
                        'Charset': CHARSET,
                        'Data': body_text,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': subject,
                },
            },
            Source=SENDER,
        )

    # Display an error if something goes wrong.
    except botocore.exceptions.ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print(f"Email sent! Message ID: {response['MessageId']}")
